#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Mixin to check if the given dl_options
    are valid
"""

from user_options_formatter.exceptions import \
    DLOptions_MissingKeyInDlOptions, \
    DlOptions_InvalidKeyType, \
    DlOptions_InvalidKeyValue, \
    DlOptions_InvalidKey


class given_options_validator_mixin:
    def validate_given_options(self):
        """
           Entrypoint to validate given options
        """
        self.validate_options_keys()
        self.validate_options_type()
        self.validate_file_format_content()

    def validate_options_keys(self):
        """
           Validate that the given options contain
           the expected keys
        """
        expected_keys = ('url', 'dl_playlist', 'file_format',
                         'user_id', 'is_anon', 'limited_user')
        for key in expected_keys:
            if key not in self.dl_options.keys():
                raise DLOptions_MissingKeyInDlOptions(key)
        for key in self.dl_options.keys():
            if key not in expected_keys:
                raise DlOptions_InvalidKey(key)

    def validate_options_type(self):
        """
            Validate that all keys in given options are
            of type 'string'
        """
        for key in self.dl_options.keys():
            if key in ('dl_playlist', 'is_anon', 'limited_user'):
                if not isinstance(self.dl_options[key], bool):
                    raise DlOptions_InvalidKeyType(key)
            elif key == 'user_id':
                if not isinstance(self.dl_options[key], int):
                    raise DlOptions_InvalidKeyType(key)
            else:
                if not isinstance(self.dl_options[key], str):
                    raise DlOptions_InvalidKeyType(key)

    def validate_file_format_content(self):
        """
            Validate that the value of the key 'file_format'
            is a valid one
        """
        expected_values = ('audio', 'video')
        key_value = self.dl_options['file_format']
        if key_value not in expected_values:
            raise DlOptions_InvalidKeyValue('file_format', key_value)
