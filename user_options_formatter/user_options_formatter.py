#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    Adapt the users options from the web form
"""

from typing import NoReturn

from user_options_formatter.validator import \
    given_options_validator_mixin


class user_options_formatter(given_options_validator_mixin):
    """
        Main class to format user options from HCI
    """
    dl_options: dict = None
    url: str = None
    file_format: str = None
    dl_playlist: bool = None
    user_id: int = None
    is_anon: bool = None
    directory_uniq_to_session: str = None

    def __init__(self, dl_options: dict):
        """
            - dl_options: (dict) list of user's options
              Content:
                |-> url: (str) youtube video url
                |-> file_format: (str) mp3/mp4
                |-> dl_playlist: (bool) uniq_dl or playlist dl
                |-> user_id: (int) user/anon id in core db
                |-> is_anon: (bool) if dl is from registered user
                |-> limited_user: (bool) if dl is from free/anon user
                                    or anonymous one
        """
        self.dl_options = dl_options

    def get_formated_options(self):
        """
            Return a list with all the formatted options
        """
        # pylint: disable=C0330
        self.format_download_options()
        return {'url': self.dl_options['url'],
                'dl_playlist': self.dl_options['dl_playlist'],
                'user_id': self.dl_options['user_id'],
                'is_anon': self.dl_options['is_anon'],
                'limited_user': self.dl_options['limited_user'],
                'dl_options': {'format': self.dl_options['file_format']}}

    def format_download_options(self) -> NoReturn:
        """
            Format all necessary options
        """
        self.validate_given_options()
