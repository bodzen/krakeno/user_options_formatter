#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = '2.2'


from user_options_formatter.user_options_formatter import \
    user_options_formatter
