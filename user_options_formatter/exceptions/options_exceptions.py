#!/usr/bin/python3
# -*- coding: utf-8 -*-

__all__ = [
    'DLOptions_MissingKeyInDlOptions',
    'DlOptions_InvalidKeyType',
    'DlOptions_InvalidKeyValue',
    'DlOptions_InvalidKey'
]


class CustomException(Exception):
    """
        Master custom Exception
    """
    def __init__(self, *args: tuple) -> None:
        error_msg = self.build_error_msg(*args)
        super().__init__(error_msg)


class DLOptions_MissingKeyInDlOptions(CustomException):
    """
        Custom Exception used when the given
        dl_options does not contain an expected key
            - parama0: (str) missing key
    """
    def build_error_msg(self, key):
        return 'The given dl_option is missing the ' \
            'key: "{0}"'.format(key)


class DlOptions_InvalidKeyType(CustomException):
    """
       Custom Exception used when a key's value
       type is invalid
    """
    def build_error_msg(self, key):
        return 'The key "{0}" in the given options ' \
            'is of type "{1}"'.format(key, type(key))


class DlOptions_InvalidKeyValue(CustomException):
    """
        Custom Exception used when the key's value
        is invalid
    """
    def build_error_msg(self, key, key_value):
        return 'The key "{0}" has an invalid ' \
            'value: "{1}"'.format(key, key_value)


class DlOptions_InvalidKey(CustomException):
    """
        Custom Exception used when an invalid key is
        passed to the constructor
    """
    def build_error_msg(self, key):
        return 'The key "{0}" is not a valid ' \
            'one.'.format(key)
