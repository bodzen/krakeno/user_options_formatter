#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore
import user_options_formatter as my_package

setup(
    name='user_options_formatter',
    version=my_package.__version__,
    author='dh4rm4',
    description='Format users options from Krakeno\'s HCI.',
    install_requires=[],
    classifiers=[
        "Programming Language :: Python 3.8+",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
