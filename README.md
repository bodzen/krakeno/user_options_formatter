# user_options_formatter

Format the user options from [HCI](https://gitlab.com/bodzen/krakeno/hci).

Currently in use by [administrator](https://gitlab.com/bodzen/krakeno/administrator)

## How to
```
>>> from user_options_formatter import user_options_formatter
>>> inst = user_options_formatter(DL_OPTIONS)
>>> formatted_options = inst.get_formated_options()
```
