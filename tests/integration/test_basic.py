#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from user_options_formatter import \
    user_options_formatter


class option_formatter_basic_tests(unittest.TestCase):
    def setUp(self):
        dl_options = {'dl_playlist': False,
                      'url': 'https://youtube.com/abcdef',
                      'file_format': 'audio',
                      'user_id': 42,
                      'is_anon': False,
                      'limited_user': False}
        self.inst = user_options_formatter(dl_options)
        self.inst.format_download_options()

    def test_returned_value_type(self):
        """
            Test the type of get_formated_options method's
            returned value
        """
        self.assertEqual(dict,
                         type(self.inst.get_formated_options()))

    def test_returned_dict_key(self):
        """
            Test if the dict return by the method
            get_formated_options contains the given key
        """
        expected_keys = ('url', 'dl_options', 'dl_playlist', 'user_id',
                         'is_anon', 'limited_user')
        options = self.inst.get_formated_options()
        self.__test_dict_key(options, expected_keys)

    def test_nested_dl_options_keys(self):
        """
            Test the keys in the nested returned dict dl_options
        """
        expected_key = ['format']
        res = self.inst.get_formated_options()
        dl_options = res['dl_options']
        self.__test_dict_key(dl_options, expected_key)

    def __test_dict_key(self, given_dict: dict, expected_keys: list):
        """
            Check the validity of the given_dict's keys
            1. if the dict's keys are in expected_keys
            2. if all expected_keys are in the given_dict
                - given_dict: (dict) dictionnary to check
                - expected_keys: (list) valid keys
        """
        err_msg = ''
        for key in given_dict.keys():
            if key not in expected_keys:
                err_msg += self.__get_dict_invalid_key(key)
        for key in expected_keys:
            if key not in given_dict.keys():
                err_msg += self.__get_dict_missing_key_msg(key)
        if len(err_msg) > 0:
            raise KeyError(err_msg)

    def __get_dict_invalid_key(self, key: str) -> str:
        return '[ERROR] Returned dict contain an invalid ' \
            'key: "{0}" | '.format(key)

    def __get_dict_missing_key_msg(self, key: str) -> str:
        return '[ERROR] Returned dict is missing a key: '\
            '"{0}" | '.format(key)
