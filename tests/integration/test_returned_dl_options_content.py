#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from parameterized import parameterized
from user_options_formatter import \
    user_options_formatter


class option_formatter_other_content_tests(unittest.TestCase):
    @parameterized.expand([
        [False, False],
        [True, True]
    ])
    def test_noplaylist_key_content(self, dl_playlist: str,
                                    expected_result: bool):
        """
            Check the validity of the returned "noplaylist"
        """
        dl_options = {'url': 'youtube.com/XXXX',
                      'dl_playlist': dl_playlist,
                      'user_id': 42,
                      'file_format': 'video',
                      'is_anon': False,
                      'limited_user': False}
        inst = user_options_formatter(dl_options)
        inst.format_download_options()
        options = inst.get_formated_options()
        self.assertEqual(options['dl_playlist'], expected_result)
