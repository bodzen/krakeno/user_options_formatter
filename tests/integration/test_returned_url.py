#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from parameterized import parameterized
from user_options_formatter import \
    user_options_formatter


class options_url_formatter_test(unittest.TestCase):
    @parameterized.expand([
        ['alpha', 'youtube.com/abcdef', 'youtube.com/abcdef'],
        ['beta', 'youtu.be/boid', 'youtu.be/boid'],
    ])
    def test_url_content(self, name: str, url: str,
                         expected_result: str):
        """
            Test the values of the key url
                - name: (str) test name
                - value: (str) value of the url key
                - is_valid: (bool) is it a valid value
        """
        dl_options = {'url': url,
                      'dl_playlist': False,
                      'file_format': 'video',
                      'user_id': 42,
                      'is_anon': False,
                      'limited_user': False}
        inst = user_options_formatter(dl_options)
        inst.format_download_options()
        options = inst.get_formated_options()
        self.assertEqual(options['url'], expected_result)
