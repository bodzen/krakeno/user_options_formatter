#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Test if the formatter handle his init
    correctly when error occured
"""

import unittest
from parameterized import parameterized
from user_options_formatter import \
    user_options_formatter
from user_options_formatter.exceptions import \
    DLOptions_MissingKeyInDlOptions, \
    DlOptions_InvalidKeyType, \
    DlOptions_InvalidKeyValue, \
    DlOptions_InvalidKey


class formatter_init_error(unittest.TestCase):
    @parameterized.expand([
        ['dl_playlist', ('file_format', 'url', 'user_id', 'is_anon', 'limited_user')],
        ['file_format', ('url', 'dl_playlist', 'user_id', 'is_anon', 'limited_user')],
        ['dl_playlist', ('url', 'file_format', 'user_id', 'is_anon', 'limited_user')],
        ['user_id', ('dl_playlist', 'url', 'file_format', 'is_anon', 'limited_user')],
        ['is_anon', ('dl_playlist', 'file_format', 'url', 'user_id', 'limited_user')],
        ['limited_user', ('dl_playlist', 'file_format', 'url', 'user_id', 'is_anon')],
    ])
    def test_missing_key(self, missing_key: str, valid_keys: tuple):
        """
            Test the raised exception when a key is missing
        """
        dl_options = {key: None for key in valid_keys}
        inst = user_options_formatter(dl_options)
        self.assertRaises(DLOptions_MissingKeyInDlOptions,
                          inst.validate_options_keys)

    @parameterized.expand([
        ['Boid'],
        ['is'],
        ['STRONG']
    ])
    def test_invalid_key(self, invalid_key: str):
        """
            Test exception raised while passing invalid
            options keys
        """
        dl_options = {'dl_playlist': 'no',
                      'url': 'https://youtube.com/abcdef',
                      'file_format': 'mp3',
                      'user_id': 42,
                      'is_anon': True,
                      'limited_user': False,
                      'invalid_key': False}
        inst = user_options_formatter(dl_options)
        self.assertRaises(DlOptions_InvalidKey,
                          inst.validate_options_keys)

    @parameterized.expand([
        ['dl_playlist', ('file_format', 'url', 'user_id', 'is_anon', 'limited_user')],
        ['file_format', ('url', 'dl_playlist', 'user_id', 'is_anon', 'limited_user')],
        ['dl_playlist', ('url', 'file_format', 'user_id', 'is_anon', 'limited_user')],
        ['user_id', ('dl_playlist', 'url', 'file_format', 'is_anon', 'limited_user')],
        ['is_anon', ('dl_playlist', 'file_format', 'url', 'user_id', 'limited_user')],
        ['limited_user', ('dl_playlist', 'file_format', 'url', 'user_id', 'is_anon')],
    ])
    def test_content_type(self, key_to_test: str, untested_keys: tuple):
        """
            Test the raised exception when the type
            of an options is invalid
        """
        dl_options = {key: 'ignored' for key in untested_keys}
        dl_options[key_to_test] = 42
        inst = user_options_formatter(dl_options)
        self.assertRaises(DlOptions_InvalidKeyType,
                          inst.validate_options_type)

    @parameterized.expand([
        ['audio', True],
        ['video', True],
        ['invalid', False],
    ])
    def test_file_format_content(self, value, is_valid):
        """
            Test the values of the key file_format
                - value: (str) value of the file_format key
                - is_valid: (bool) is it a valid value
        """
        dl_options = {'file_format': value}
        inst = user_options_formatter(dl_options)
        if is_valid:
            self.assertEqual(inst.validate_file_format_content(), None)
        else:
            self.assertRaises(DlOptions_InvalidKeyValue,
                              inst.validate_file_format_content)
